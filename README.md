# E-AUCTION@IITB
This is a Project which deals with an Online Auction System. We have built a small system that can take care of Auctions on a small scale.
Like the buying and selling of second hand Goods within a university campus.

To start in a local server :  
  go to SW_LAB_IITB/auctionsonline/
  run : python3 manage.py runserver
  In the URL in a browser see for : http://127.0.0.1:8000/website

It has also been hosted at the web server :
  search for gourabdipta.pythonanywhere.com/website
  
 Details for hosting can be found in the Developer Documentation.
